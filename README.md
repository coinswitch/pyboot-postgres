# pyboot
This is bootstrap code for a new python, sqlalchemy web project project


### How to create new version
    1. Take pull from master
    2. create a new feature branch from master
    3. make your changes at you feature branch
    4. create PR to stage for stage testing
    5. if everything is good, then merge your feature branch to master
    6. create a tag of incremental version and update the version in setup.py
    7. done