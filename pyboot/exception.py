class NotFoundException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class InvalidInputException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class InvalidValueException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class DuplicateValueException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class InvalidStateException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class AuthFailedException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class AccessDeniedException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class MultipleRowsFoundException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class ConditionalCheckFailedException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class LegalReasonNotAllowedException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class LimitExceededException(Exception):
    def __init__(self, message=None):
        super().__init__(message)


class HttpError(Exception):
    def __init__(self, message=None):
        super().__init__(message)

class QueryTimeoutException(Exception):
    def __init__(self, message=None):
        super().__init__(message)

class GenericApiException(Exception):
    """
    Code and  msg both are string
    """
    def __init__(self, msg:str=None, code:str=None):
        self.code = code
        self.msg = msg
        super().__init__("code:{} msg: {}".format(self.code, self.msg))

    def __str__(self):
        return "GenericApiException code: {} msg: {}".format(self.code, self.msg)