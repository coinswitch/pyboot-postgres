import os
import logging
from functools import wraps
import newrelic.agent

from flask import render_template
from flask import request
from pyboot.api_model import ExternalApiResponseWrapper
from werkzeug.exceptions import BadRequest

from pyboot.exception import NotFoundException, InvalidInputException, InvalidValueException, \
    AccessDeniedException, DuplicateValueException, InvalidStateException, AuthFailedException, \
    ConditionalCheckFailedException, LegalReasonNotAllowedException, LimitExceededException, GenericApiException
from pyboot.model import HttpResponse
from pyboot.json import json_response


def response_handler():
    def decorator(f):
        @wraps(f)
        def response_handler(*args, **kwargs):
            if request.path.startswith("/api/"):
                try:
                    return f(*args, **kwargs)
                except (BadRequest, InvalidInputException, InvalidValueException, InvalidStateException) as e:
                    logging.warning("Bad request [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=400, message=str(e))), 400
                except NotFoundException as e:
                    logging.warning("Not found [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=404, message=str(e))), 404
                except DuplicateValueException as e:
                    logging.warning("Duplicate value [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=409, message=str(e))), 409
                except AccessDeniedException as e:
                    logging.warning("Access denied (Forbidden) [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=403, message="Access denied (Forbidden)")), 403
                except ConditionalCheckFailedException as e:
                    logging.warning("Failed to acquire optimistic lock [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=412, message=str(e))), 412
                except Exception as e:
                    logging.error("Internal error [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=500, message="Internal error")), 500
            else:
                try:
                    return f(*args, **kwargs)
                except NotFoundException as e:
                    logging.warning("Not found [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return render_template("error/server_error.html", error=str(e)), 404
                except (InvalidInputException, InvalidValueException, InvalidStateException) as e:
                    logging.warning("Bad request [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return render_template("error/server_error.html", error=str(e)), 400
                except AccessDeniedException as e:
                    logging.warning("Access denied (Forbidden) [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return render_template("error/access_denied.html"), 403
                except Exception as e:
                    logging.error("Internal error [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return render_template("error/server_error.html"), 500

        return response_handler

    return decorator

class Decorator(object):
    def init(self):
        pass


class Controller(Decorator):
    def __init__(self):
        self.not_found_template = None
        self.server_error_template = None
        self.access_denied_template = None

    def view_controller(self):
        def decorator(f):
            @wraps(f)
            def view_response_handler(*args, **kwargs):
                try:
                    return f(*args, **kwargs)
                except NotFoundException as e:
                    logging.warning("Not found [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    if self.not_found_template:
                        return render_template(self.not_found_template, exception=e), 404
                    else:
                        return "Not found [404]: " + str(e)
                except (InvalidInputException, InvalidValueException, InvalidStateException) as e:
                    logging.warning("Bad request [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    if self.server_error_template:
                        return render_template(self.server_error_template, exception=e), 400
                    else:
                        return "Server error [500]: " + str(e)
                except AccessDeniedException as e:
                    logging.warning("Access denied (Forbidden) [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    if self.access_denied_template:
                        return render_template(self.access_denied_template, exception=e), 403
                    else:
                        return "Access Denied [403]: " + str(e)
                except Exception as e:
                    logging.error("Internal error [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    if self.server_error_template:
                        return render_template(self.server_error_template, exception=e), 500
                    else:
                        return "Server error [500]: " + str(e)

            return view_response_handler

        return decorator

    def api_controller(self):
        def decorator(f):
            @wraps(f)
            def api_response_handler(*args, **kwargs):
                try:
                    response = f(*args, **kwargs)
                    return response if response else json_response(HttpResponse())
                except (BadRequest, InvalidInputException, InvalidValueException, InvalidStateException) as e:
                    logging.warning("Bad request [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=400, message=str(e))), 400
                except NotFoundException as e:
                    logging.warning("Not found [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=404, message=str(e))), 404
                except DuplicateValueException as e:
                    logging.warning("Duplicate value: [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=409, message=str(e))), 409
                except AccessDeniedException as e:
                    logging.warning("Access denied (Forbidden) [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=403, message=str(e))), 403
                except AuthFailedException as e:
                    logging.warning("Authentication Failed [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=401, message=str(e))), 401
                except ConditionalCheckFailedException as e:
                    logging.warning("Failed to acquire optimistic lock [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=412, message=str(e))), 412
                except LegalReasonNotAllowedException as e:
                    logging.warning("Not allowed for legal reason [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=451, message=str(e))), 451
                except LimitExceededException as e:
                    logging.warning("Max limit reached [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=429, message=str(e))), 429
                except Exception as e:
                    logging.error("Internal error [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(HttpResponse(code=500, message="Internal error")), 500

            return api_response_handler

        return decorator

    def external_api_controller(self):
        def decorator(f):
            @wraps(f)
            def api_response_handler(*args, **kwargs):
                try:
                    response = f(*args, **kwargs)
                    return response if response else json_response(HttpResponse())
                except GenericApiException as e:
                    if os.environ.get("NEW_RELIC")=="enable":
                        newrelic.agent.record_exception(params={"code": e.code, "msg": e.msg, "success": False})
                    logging.warning("ExternalApi GenericApiException [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    return json_response(ExternalApiResponseWrapper(success=False, code=e.code, msg=e.msg), status=200)
                except Exception as e:
                    logging.error("ExternalApi Unknown Exception [%s %s]: %s" % (request.method, request.url, e), exc_info=1)
                    msg = "Internal Error"
                    if os.environ.get("NEW_RELIC") == "enable":
                        newrelic.agent.record_exception(params={"code": "unknown", "msg": msg, "success": False})
                    return json_response(ExternalApiResponseWrapper(success=False, code="unknown", msg=msg), status=200)
            return api_response_handler

        return decorator
