from contextlib import contextmanager
import datetime
import logging
from logging import config
import yaml

from sqlalchemy import create_engine
from sqlalchemy import event
from sqlalchemy import exc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool, QueuePool

from pyboot.util import DateTimeUtil
from pyboot.exception import QueryTimeoutException

MIME_TYPE_JSON = "application/json"

log = logging.getLogger(__name__)

def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
    timeout = conn._execution_options.get("query_timeout") or conn._execution_options.get("default_query_timeout")

    if timeout is not None:
        try:
            timeout_statement = f"SET LOCAL statement_timeout={timeout}"  # Timeout in milliseconds
            cursor.execute(timeout_statement)
        except Exception as e:
            log.error(f"Exception while running query {e}")

class Conf(object):
    def __init__(self, app_conf_file, logger_conf_file, host_type=None):
        self.app_conf_file = app_conf_file
        self.logger_conf_file = logger_conf_file
        self.host_type = host_type
        self.app_conf = None

    def init(self):
        if self.logger_conf_file:
            stream = open(self.logger_conf_file, "r")
            config_dict = yaml.load(stream)

            if logging.root.handlers:
                log.debug("Existing root handlers found. Not setting new root logger {}".format(config_dict['root']))
                del config_dict['root']

            config.dictConfig(config_dict)
            stream.close()
            log.debug("Logging initialized")

        stream = open(self.app_conf_file, "r")
        self.app_conf = yaml.load(stream)
        stream.close()
        log.debug("Config initialized")
        return self

    def get(self, key, default=None):
        if key not in self.app_conf: return default
        return self.app_conf[key]

    def set(self, key, value):
        self.app_conf[key] = value


class Db(object):
    def __init__(self, host=None, port=3306, username=None, password=None, db_name=None, charset="utf8",
                 init_pool_size=1, max_pool_size=5, pool_recycle_delay=600, sql_logging=False):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        self.charset = charset
        self.init_pool_size = init_pool_size
        self.max_pool_size = max_pool_size
        self.pool_recycle_delay = pool_recycle_delay
        self.sql_logging = sql_logging
        self.__Session = None

    def init(self):
        self.__Session = sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=self.get_engine())
        log.debug("DbConfig initialized")
        return self

    def get_engine(self):
        db_baseurl = "mysql://%s:%s@%s:%s/%s?charset=%s" % (
            self.username, self.password, self.host, self.port, self.db_name, self.charset)
        log.info("DB Baseurl: %s, Init pool size: %s, Max pool size: %s, Pool recycle delay: %s" % (
            db_baseurl, self.init_pool_size, self.max_pool_size, self.pool_recycle_delay))
        return create_engine(db_baseurl, echo=self.sql_logging, poolclass=QueuePool, pool_size=self.init_pool_size,
                             max_overflow=int(self.max_pool_size) - int(self.init_pool_size),
                             pool_recycle=int(self.pool_recycle_delay))

    def __get_session(self):
        return self.__Session()

    @contextmanager
    def get(self):
        start_time = datetime.datetime.now()
        db = self.__get_session()
        log.debug("Connection time: %s milliseconds" % DateTimeUtil.diff(start_time, datetime.datetime.now()))
        try:
            yield db
        except:
            db.rollback()
            raise
        finally:
            db.close()


class DBQueryHandler(object):
    @staticmethod
    def get(db, query, one=False):
        cur = db.execute(query)
        rv = cur.fetchall()
        cur.close()
        rows = []
        for row in rv:
            rows.append(dict(zip(row.keys(), row)))

        return (rows[0] if rows else None) if one else rows

    @staticmethod
    def get_all(db, query, one=False):
        cur = db.execute(query)
        rv = cur.fetchall()
        cur.close()
        rows = []
        for row in rv:
            rows.append(row[0])
        return (rows[0] if rows else None) if one else rows

    @staticmethod
    def update(db, query):
        cur = db.execute(query)
        cur.close()

class PostgresDb(object):
    def __init__(self, host=None, port=5432, username=None, password=None, db_name=None, charset="utf8",
                 init_pool_size=1, max_pool_size=5, pool_recycle_delay=600, sql_logging=False,
                 application_name=None, pool_pre_ping=False, query_timeout=None):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name
        self.charset = charset
        self.init_pool_size = init_pool_size
        self.max_pool_size = max_pool_size
        self.pool_recycle_delay = pool_recycle_delay
        self.sql_logging = sql_logging
        self.application_name = application_name
        self.pool_pre_ping = pool_pre_ping
        self.__Session = None
        self.query_timeout = query_timeout

    def init(self):
        self.__Session = sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=self.get_engine())
        log.debug("DbConfig initialized")
        return self

    def get_engine(self):
        db_baseurl = "postgresql://%s:%s@%s:%s/%s?client_encoding=%s" % (
            self.username, self.password, self.host, self.port, self.db_name, self.charset)
        log.debug("end of engine")
        connect_args = {}
        if self.application_name:
            connect_args = {"application_name": self.application_name}
        self.__engine = create_engine(db_baseurl, echo=self.sql_logging, poolclass=NullPool, connect_args=connect_args,pool_pre_ping=self.pool_pre_ping)
        event.listen(self.__engine, 'before_cursor_execute', before_cursor_execute)
        return self.__engine

    def dispose_engine(self):
        self.__engine.dispose()

    def __get_session(self):
        return self.__Session()

    @contextmanager
    def get(self, timeout=None):
        start_time = datetime.datetime.now()
        db = self.__get_session()
        log.debug("Connection time: %s milliseconds" % DateTimeUtil.diff(start_time, datetime.datetime.now()))

        query_timeout = timeout if timeout is not None else None
        db.connection(execution_options={"default_query_timeout": self.query_timeout, "query_timeout": query_timeout})

        try:
            yield db
        except exc.OperationalError as e:
            log.error(f"Query timed out due to exception {e} having query timeout: {query_timeout}")
            # Handle the query timeout situation
            raise QueryTimeoutException("Query was canceled due to a statement timeout")
        except Exception as e:
            log.exception(f'pyboot yield db Exception: {e}')
            db.rollback()
            raise
        finally:
            db.close()


class PostgresDBQueryHandler(object):
    @staticmethod
    def get(db, query, one=False):
        try:
            cur = db.execute(query)
            rv = cur.fetchall()
            cur.close()
            rows = []
            for row in rv:
                rows.append(dict(zip(row.keys(), row)))

            return (rows[0] if rows else None) if one else rows
        except Exception as e:
            log.error(f"Exception while running query {e}")
            raise

    @staticmethod
    def get_query(db, query, **params):
        try:
            cur = db.execute(query, params)
            rv = cur.fetchall()
            cur.close()
            rows = []
            for row in rv:
                rows.append(dict(zip(row.keys(), row)))
            return rows
        except Exception as e:
            log.error(f"Exception while running query {e}")
            raise

    @staticmethod
    def get_all(db, query, one=False):
        try:
            cur = db.execute(query)
            rv = cur.fetchall()
            cur.close()
            rows = []
            for row in rv:
                rows.append(row)
            return rows
        except Exception as e:
            log.error(f"Exception while running query {e}")
            raise

    @staticmethod
    def update(db, query):
        try:
            cur = db.execute(query)
            cur.close()
        except Exception as e:
            log.error(f"Exception while running query {e}")
            raise