import six

from pyboot.model import Model


class ExternalApiResponseWrapper(Model):
    _structure = {
        "success": bool,
        "code": str,
        "data": object,
        "msg": str,
    }

    def __init__(self, **kwargs):
        self.success = False
        self.code = ""
        self.data = None
        self.msg = ""
        for key, value in six.iteritems(kwargs):
            setattr(self, key, value)
