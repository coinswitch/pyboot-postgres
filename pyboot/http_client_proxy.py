import requests
from requests.exceptions import ProxyError, Timeout
import logging

COUNT = -1


def get_proxy_config(proxy_list):
    global COUNT
    COUNT += 1
    COUNT = COUNT % len(proxy_list)
    proxy_config = {'http': proxy_list[COUNT], 'https': proxy_list[COUNT]}
    logging.info(f"Proxy used: {proxy_list[COUNT]}")
    return proxy_config


class HttpClientProxy(object):
    def __init__(self, proxy_list):
        self.proxy_list = proxy_list
        pass

    def make_api_call(self, method, url, **kwargs):
        """
        This method is used to send requests via proxy servers.

        :param method: method for the new :class:`Request` object.
        :param url: url: URL for the new :class:`Request` object.
        :param kwargs:
        :return: :class:`Response <Response>` object
        """

        proxies = get_proxy_config(self.proxy_list)
        try:
            r = requests.request(method, url, proxies=proxies, **kwargs)
        except (ProxyError, Timeout, requests.exceptions.ConnectionError) as e:
            exc_name = type(e).__name__
            logging.warning(f"Try1: {exc_name} received with proxies={proxies}. Trying with another proxy", exc_info=1)
            proxies = get_proxy_config(self.proxy_list)
            try:
                r = requests.request(method, url, proxies=proxies, **kwargs)
            except (ProxyError, Timeout, requests.exceptions.ConnectionError) as e:
                exc_name = type(e).__name__
                logging.warning(f"Try2: {exc_name} again received with proxies={proxies}. Trying without proxy", exc_info=1)
                r = requests.request(method, url, **kwargs)

        r.raise_for_status()
        return r
